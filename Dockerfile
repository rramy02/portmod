FROM docker.io/archlinux

MAINTAINER Portmod

USER root
RUN pacman -Sy rust bubblewrap python python-pip git gcc make patch python-virtualenv unzip pandoc pkgconf icu which wget cargo-audit --noconfirm
